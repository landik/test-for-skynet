<?php
defined('TRUE_ENTRY') or die('404') ;

function valid_path($path = ""){
    if(!mb_strlen($path)) return false;

    // Валидный путь
    $separator = (DS == '/')? '\\': DS;

    return str_replace($separator,DS, $path);
}
/*
 * Моя стилизованная версия var-dump
 * */
function my_var_dump($data){
    echo '<ul class="my-var-dump">';
    $type = gettype($data);
    echo "<li>({$type})</li>";
    switch ($type){
        case "object":
        case "array":
            foreach ($data as $key => $val){
                echo "<li>";
                if(gettype($val) == "integer" || gettype($val) == "string" || gettype($val) == "boolean"){
                    echo "[{$key}] => (".gettype($val).") ".$val;
                }
                else{
                    echo "[{$key}] \/";
                    my_var_dump($val);
                }
                echo "</li>";
            }
            break;
        default:
            echo '<li>'.$data.'</li>';
            break;


    }
    echo '</ul>';
}