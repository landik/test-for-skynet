<?php
defined('TRUE_ENTRY') or die('404') ;

class Tarif{

    private $tarifs;

    public function __construct()
    {
        $path = (TEST_MODE)? PATH_WITH_JSON_LOCAL : PATH_WITH_JSON_REMOTE;
        $data = json_decode(file_get_contents($path));
        if($data->result != "ok") return false;
        $this->tarifs = $data->tarifs;
        $this->sotrTartifForPayMonth();
        $this->addPriceDiscount();
        $this->addDiscount();
        $this->convertDate();
    }

    public function updateTarif($newTarif){
        foreach ($this->tarifs as $key=>$item){
            if($item->title == $newTarif->title) {
                $this->tarifs[$key] = $newTarif;
                break;
            }
        }
    }

    /**
     * Добавление цены за месяц к тарифам с ценой за несколько месяцев
     * И добавление минимальной и максимальной цены для тарифа
     */
    private function addPriceDiscount(){
        foreach ($this->tarifs as &$item){
            $max=0;
            $min=-1;
            foreach ($item->tarifs as $key=>&$tarif){
                $tarif->pay_one_month =$tarif->price/$tarif->pay_period;
                if($max == 0 || $max<$tarif->pay_one_month) $max = $tarif->pay_one_month;
                if($min == -1 || $min>$tarif->pay_one_month) $min = $tarif->pay_one_month;
            }
            $item->price_min = $min;
            $item->price_max = $max;
        }
    }

    private function addDiscount(){
        foreach ($this->tarifs as &$item){
            foreach ($item->tarifs as $key=>&$tarif){
                $tarif->discount = $tarif->pay_period*$item->price_max - $tarif->price;
            }
        }

    }

    private function convertDate(){
        foreach ($this->tarifs as &$item){
            foreach ($item->tarifs as $key=>&$tarif){
                $date = explode('+',$tarif->new_payday);
                $tarif->new_payday = date('d.m.Y',$date[0]);
            }
        }
    }

    /**
     * Сортировка массива тарифов плана по месяцам, от 1 до 12
     */
    private function sotrTartifForPayMonth(){
        foreach ($this->tarifs as &$item) {
            $tarifs = &$item->tarifs;
            for ($i = 0; $i < count($tarifs); $i++) {
                for ($j = 0; $j < count($tarifs); $j++) {
                    if ($tarifs[$i]->pay_period < $tarifs[$j]->pay_period) {
                        $p = $tarifs[$i];
                        $tarifs[$i] = $tarifs[$j];
                        $tarifs[$j] = $p;
                    }
                }
            }
        }
        return $tarifs;
    }

    public function getListTarifs(){
        return $this->tarifs;
    }
}