<?php
defined('TRUE_ENTRY') or die('404') ;
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>SkyNet</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="src/css/style.css">
    <style type="text/css">
        .my-var-dump {
            display: block !important;
            padding-left: 30px !important;
        }
        .my-var-dump > li > ul {
            overflow: scroll !important;
            /*height: 0 !important;*/
            transition: all 0.3s ease-out !important;
        }
        .my-var-dump > li:hover > ul {
            /*height: 200px !important;*/
        }
    </style>
</head>
<body>
<div id="navigation">
    <div  class="navigation navigation_hide">
        <div class="navigation__back-arrow"></div>
        <div class="navigation__title">Выберите тариф</div>
    </div>
</div>
<div class="content">
    <div class="content__centre">
        <div id="articles">
            <div class="articles">
                <?php foreach ($tarifList as &$item):
                    switch ($item->speed){
                        case 50:
                            $color = "brown";
                            break;
                        case 100:
                            $color = "blue";
                            break;
                        case 200:
                            $color = "orange";
                            break;
                    }
                    ?>
                    <article class="articles__item">
                        <header class="articles__row">
                            <h2 class="articles__title">Тариф "<?=$item->title?>"</h2>
                        </header>
                        <div class="articles__row articles__row_arrow">
                            <a class="tarif tarif_article" href="#">
                                <div class="tarif__speed tarif__speed_<?=$color?>"><?=$item->speed?> Мбит/с</div>
                                <div class="tarif__price"><?=$item->price_min." - ".$item->price_max?> руб/мес</div>
                                <?php if(isset($item->free_options)):?>
                                <ul class="tarif__info">
                                    <?php foreach ($item->free_options as &$option):?>
                                        <li><?=$option?></li>
                                    <?php endforeach;?>
                                </ul>
                                <?php endif;?>
                            </a>
                        </div>
                        <div class="articles__row">
                            <a href="<?=$item->link?>" class="color_blue">Узнать подробнее на сайте www.sknt.ru</a>
                        </div>
                    </article>
                <?php endforeach;?>
            </div>
        </div>
    </div>
</div>
</body>
</html>