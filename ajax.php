<?php
define('DS',DIRECTORY_SEPARATOR);
define('HOME_DIR',__DIR__);
define('HOME_DIR_DS',__DIR__.DS);
define('PATH_WITH_JSON_LOCAL', __DIR__.DS."data.json");
define('PATH_WITH_JSON_REMOTE', "http://sknt.ru/job/frontend/data.json");
define('TEST_MODE',false);
define('TRUE_ENTRY',true);
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set("allow_url_fopen", 1);
global $input;
$input="";

include HOME_DIR_DS."modules".DS."functions.php";
include HOME_DIR_DS.valid_path("modules/controller.php");